# Documentation de l'image docker

## Contenu
Ce Dockerfile sert à créer une image contenant le nécessaire pour un conteneur capable de servir des webservices basés sur obspy et flask.

Le serveur web est [http://gunicorn.org/](gunicorn)

### Dockerfile

Le système est asser simple, l'idée étant de rendre disponible le code de l'application à partir d'un volume docker. Le volume est déclaré sur /app

### Dossier setup

Contient le système de configuration pour gunicorn ainsi que les dépendances pythons nécessaires.

Un changement dans ce répertoire doit être suivi d'un build de l'image docker.

#### gunicorn.conf

Ce script python, permet de passer à gunicorn des paramètres de configuration au moyen de variable d'environnement docker.

Toutes les configurations de gunicorn sont disponibles par le pattern "-e GUNICORN_OPTION=valeur", qui sera transformé pour gunicorn en "--option=valeur"

Exemples :

  - `GUNICORN_RELOAD=True` : lancer l'application en mode "reload automatique"
  - `GUNICORN_BIND="0.0.0.0:80` : lier l'application au port 80 dans le conteneur
  - `GUNICORN_WORKERS="7"` : lancer 7 workers pour l'application

Liste complète des paramètres : http://docs.gunicorn.org/en/latest/settings.html

Pour la réalisation, nous nous sommes inspiré de [https://sebest.github.io/post/protips-using-gunicorn-inside-a-docker-image/](cet article).

#### requirements.txt

Contient toutes les dépendances python.

## Construction de l'image

Pour créer une image, il faut être root et taper la commande suivante :

  docker build -t resif-guniflaskobspy:20180801 .

## Lancer un webservice

```
docker run -d -p 9085:8000 -v /home/sysop/webservices/ws-timeseriesplot:/app --name ws-timeseriesplot -e RUNMODE=production -e GUNICORN_BIND=0.0.0.0:8000  resif-gunicorn:20180801 start:app
```

L'application sera accessible sur le port 9085 de la machine hébergeant les containers. Elle utilise le RUNMODE de production, et écoute le port local 8000.

Détails de la commande :

  - `-d` : tourne en background
  - `-p 9085:8000` : le port 9085 du serveur hôte est mappé sur le port 8000 du conteneur
  - `-v /home/sysop/webservices/ws-timeseriesplot:/app` : le dossier local du webservice est monté dans le container sur `/app`
  - `--name ws-timeseriesplot` : le nom de cette instance docker
  - `-e RUNMODE=production` : l'application recevra la variable d'environnement `RUNMODE=production`
  - `-e GUNICORN_BIND=0.0.0.0:8000` : le process gunicorn reçoit l'option `--bind=0.0.0.0:8000`
  - `resif-gunicorn:20180801` : le nom de l'image à utiliser pour cette instance docker
  - `start:app` : Dans le Dockerfile, la dernière ligne CMD peut être surchargée par un paramètre. Ici, on indique `start:app` à la place de `main:app`.

### Indiquer la bonne application à démarrer

Dans notre exemple ci dessus, l'application démarrée est `start:app`, cela signifie que le fichier de démarrage flask est `start.py`.

Dans le Dockerfile, la combinaison des lignes `ENTRYPOINT` et `CMD` permet de surchager le paramètre indiqué dans `CMD` sur la commande `docker run`. Ainsi, si notre fichier principal flask s'appelle `application.py`, on peut le lancer avec :

```
docker run resif-gunicorn:20180730 application:app
```
