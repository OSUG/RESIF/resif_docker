# Docker rsync-resif

Ce dockerfile peut être utilisé pour déployer le service rsync.resif.fr

Il faut y attacher une configuration rsync par le fichier `/etc/rsyncd.conf` et le répertoire `/etc/rsyncd.d`.

Et on y attache les volumes de partage nécessaires également.

## Construction de l'image

    docker build -t gricad-registry.univ-grenoble-alpes.fr/osug/resif/resif_docker/rsync-resif:$(date '+%Y%m%d') .
    docker login gricad-registry.univ-grenoble-alpes.fr
    docker push gricad-registry.univ-grenoble-alpes.fr/osug/resif/resif_docker/rsync-resif:$(date '+%Y%m%d')
