Pour utiliser ce docker dans une application :

# Le container qui exécutera le code.
# On y copie les sources de PH5, et tous les packages qui ont été compilés précédemment
# On installe python2.7 pour que PH5 fonctionne
FROM gricad-registry.univ-grenoble-alpes.fr/osug/resif/resif_docker/ph5 as ph5-build
FROM python:3.8-slim
RUN apt-get update && apt-get install -y python-pip
COPY --from=ph5-build /PH5 /PH5
COPY --from=ph5-build /usr/local/lib/python2.7/site-packages /usr/lib/python2.7/dist-packages
RUN pip2 install numpy
RUN pip2 install -e /PH5
CMD ["sleep", "3600"]

