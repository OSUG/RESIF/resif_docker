# Monitoring des snapshots summer dans un docker

Pour construire le docker :

1. éditer le fichier `config.yml` et placer tous les mots de passe
2. construire le container `docker build -t mosusnap .`
3. lancer le container, par exemple par un cron `docker run --rm mosusnap`
