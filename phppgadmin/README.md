Original :
https://github.com/ZHAJOR/Docker-Phppgadmin

## Usage

First make the image :  
`docker build -t docker-phppgadmin .`  
Then a container :  
`docker run -d -p 12345:80 -e "DB_HOST=192.168.99.100" -e "DB_PORT=5432"  --name=postg docker-phppgadmin`

Replace DB_HOST by your ip or the virtualhost made by --link or networks, for example:
`docker run -d --name=postgres-db postgres`  
`docker run -d -p 12345:80 --link postgres-db:postgres-db -e "DB_HOST=postgres-db" -e "DB_PORT=5432"  --name=postg docker-phppgadmin`

You can now access the phppgadmin interface from http://127.0.0.1:12345/
