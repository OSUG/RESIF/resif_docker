
 
    Au demarrage  du webservice RESIFSI  trois connexions à la base de données sont ouverts. Avec le deploiement du docker et avec les configs par défauts 6 processus sont lancé ce qui revien à 18 connexions à la base de données ( 3 * 6 ). Pour déminuer  ce nombre la configuration Http.conf modifié les valeurs par defaut du mode prefork de MPM coome ceci:
 
     StartServers 0
     MinSpareServers 2 // par défaut 5 
     MaxSpareServers 3 // max de process inactif

