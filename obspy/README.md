# Docker for obspy

Pour générer cette image et la publier sur le registry de gricad :

```
docker login gricad-registry.univ-grenoble-alpes.fr -u schaeffj
docker build -t gricad-registry.univ-grenoble-alpes.fr/osug/resif/resif_docker/obspy:python-3.8-obspy-1.2.1 .
docker push gricad-registry.univ-grenoble-alpes.fr/osug/resif/resif_docker/obspy:latest
```
